from colorama import init, Fore, Back, Style

def terminalWelcome(input_file:str, output_name:str):
    init()
    print(Style.BRIGHT + Fore.CYAN + 'Input PLUTO file path:  '+ Fore.GREEN + input_file + '\n' + Fore.CYAN + 'Output file: ' + Fore.GREEN + output_name)
    print(Style.RESET_ALL)

def LibreCubeMsg():
    print(Fore.MAGENTA + Style.BRIGHT + 'LibreCube\'s PLUTO to PDF Generator '+ Style.RESET_ALL)

def inputArgsError():
    print(Fore.YELLOW + Style.BRIGHT+"\n Input file path and output name are required."+Style.RESET_ALL)