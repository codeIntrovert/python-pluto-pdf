from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet
from reportlab.platypus import SimpleDocTemplate, Paragraph, Image, PageBreak, Paragraph
from reportlab.lib import colors
from colorama import init, Fore, Back, Style

def create_cover_page():
    print(Fore.CYAN + "Generating cover page..." + Style.RESET_ALL)
    elements = []


    image_path = "assets/logo_librecube.png"
    image = Image(image_path, width=200, height=36)  # Adjust width and height as needed
    elements.append(image)

    cover_style = ParagraphStyle(
        name='Cover', 
        fontSize=8, 
        leading=14, 
        alignment=1,
        textColor='#36454F',
        fontName='Helvetica-Bold',
        spaceBefore=10)
    elements.append(Paragraph("This is an Auto Generated PDF", cover_style))
    elements.append(Paragraph("Made using PLUTO to PDF Generator", cover_style))
    return elements
